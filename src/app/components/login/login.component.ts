import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Fade } from 'src/app/animations/fade';
import { ConfirmDialogComponent } from 'src/app/dialogs/confirm-dialog/confirm-dialog.component';
import { ErrorDialogComponent } from 'src/app/dialogs/error-dialog/error-dialog.component';
import { IdentificationDialogComponent } from 'src/app/dialogs/identification-dialog/identification-dialog.component';
import { InformationDialogComponent } from 'src/app/dialogs/information-dialog/information-dialog.component';
import { SavePersonneComponent } from 'src/app/dialogs/save-personne/save-personne.component';
import { AppConfigService } from 'src/app/services/app-config.service';
import { AuthentificationServiceService } from 'src/app/services/authentification-service.service';
import { DeclarationService } from 'src/app/services/declaration.service';
import { DeclarationBoxComponent } from '../declaration-box/declaration-box.component';
import { ResumeDeclarationComponent } from '../resume-declaration/resume-declaration.component';
import { ConsultDeclarationDialogComponent } from 'src/app/dialogs/consult-declaration-dialog/consult-declaration-dialog.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  animations: [Fade]
})
export class LoginComponent implements OnInit {

  data = {
    username: '',
    password: '',
  }

  message: string

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private appConfig: AppConfigService,
    private service: AuthentificationServiceService,
    private declarationService: DeclarationService
  ) { }

  ngOnInit(): void {
  }

  onSubmit(): void{
    this.service.connexion(JSON.stringify(this.data))
    .then(
      result => {
        this.appConfig.onConnected(result.response);
        this.router.navigate(['/main'])
      }
    ).catch(
      error => this.message = error
    )

    // this.declarationService.recupererDossier(
    //   JSON.stringify(
    //     {
    //       username: 'ruben',
    //       token: '123456',
    //       numeroDossier: 'DDLMB0000120'
    //     }
    //   )
    // )
    // .then(
    //   result => {
    //     this.dialog.open(ConsultDeclarationDialogComponent, {
    //       data: result.response,
    //       width: '90%', height: '90%'
    //     })
    //   }
    // )
    
  }

}
