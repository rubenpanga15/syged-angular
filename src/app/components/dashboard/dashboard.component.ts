import { Component, OnInit } from '@angular/core';
import { Fade } from 'src/app/animations/fade';
import { AppConfigService } from 'src/app/services/app-config.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  animations: [Fade]
})
export class DashboardComponent implements OnInit {

  constructor(
    private appConfig: AppConfigService
  ) { }

  ngOnInit(): void {
  }

  hasRight(roles: string[]):boolean{
    let right: boolean = false;

    for(let i = 0; i < roles.length; i++){
      if(roles[i] === this.appConfig.getUserConnected().role) right = true;
    }

    return right;
  }

}
