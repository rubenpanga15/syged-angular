import { Component, Input, OnInit } from '@angular/core';
import { ActeNaissance } from 'src/app/models/acte-naissance';
import { Declaration } from 'src/app/models/declaration';

@Component({
  selector: 'app-acte-naissance-view-box',
  templateUrl: './acte-naissance-view-box.component.html',
  styleUrls: ['./acte-naissance-view-box.component.css']
})
export class ActeNaissanceViewBoxComponent implements OnInit {

  @Input() acte: ActeNaissance
  @Input() declaration: Declaration

  constructor() { }

  ngOnInit(): void {
  }

}
