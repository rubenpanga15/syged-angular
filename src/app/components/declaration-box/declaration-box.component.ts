import { Component, Input, OnInit } from '@angular/core';
import { Declaration } from 'src/app/models/declaration';
import { AppConfigService } from 'src/app/services/app-config.service';
import { DeclarationService } from 'src/app/services/declaration.service';
import { MatDialog } from '@angular/material/dialog';
import { Utility } from 'src/app/utilities/utility';

@Component({
  selector: 'app-declaration-box',
  templateUrl: './declaration-box.component.html',
  styleUrls: ['./declaration-box.component.css']
})
export class DeclarationBoxComponent implements OnInit {

  @Input() declaration: Declaration

  data = {
    username: undefined,
    token: undefined,
    numeroDeclaration: undefined,
    avis: '',
    observation: '',
    numeroRegistre: undefined
  }

  constructor(
    private appConfig: AppConfigService,
    private service: DeclarationService,
    private dialog: MatDialog
  ) { 
    this.data.username = appConfig.getUserConnected().username;
    this.data.token = appConfig.getUserConnected().token;
    
    
  }

  ngOnInit(): void {
    this.data.numeroDeclaration = this.declaration.numero;
  }

  aviser():void{
    console.log(this.data)

    if(this.data.avis.length <= 0){
      Utility.openInfoDialog(this.dialog, 'Veuillez selectionner un avis');
      return;
    }

    if(this.data.observation.length <= 0){
      Utility.openInfoDialog(this.dialog, 'Veuillez entrer une observation');
      return;
    }

    if(this.data.numeroRegistre.length <= 0){
      Utility.openInfoDialog(this.dialog, 'Veuillez entre le numero de registre');
      return;
    }

    Utility.openConfirmDialog(this.dialog, 'Êtes-vous sûre de vouloir aviser ce dossier?').afterClosed()
    .subscribe(
      response => {
        if(response === 'OK'){
          this.service.aviserDeclaration(JSON.stringify(this.data)).then(
            result => {
              
              this.declaration.fkPrepose = this.appConfig.getUserConnected().id;
              this.declaration.avis = this.data.avis
              this.declaration.observationPrepose = this.data.observation;
              this.declaration.prepose = this.appConfig.getUserConnected();
      
              Utility.openSuccessDialog(this.dialog, result.error.errorDescription);
            }
          ).catch(
            error => {
      
              Utility.openErrorDialog(this.dialog, error)
            }
          )
        }
      }
    )
  }

  authentifier():void{
    console.log(this.data);

    if(this.data.observation.length <= 0){
      Utility.openInfoDialog(this.dialog, 'Veuillez entrer une observation');
      return;
    }

    Utility.openConfirmDialog(this.dialog, 'Êtes-vous sûre de vouloir authentifier cet acte?').afterClosed()
    .subscribe(
      result => {
        if(result === 'OK'){
          this.service.authentifierActe(JSON.stringify(this.data)).then(
            
            result => {
              this.declaration.fkOec = this.appConfig.getUserConnected().id;
              this.declaration.observationOec = this.data.observation;
              this.declaration.oec = this.appConfig.getUserConnected();
              Utility.openSuccessDialog(this.dialog, result.error.errorDescription)
            }
          ).catch(
            error => Utility.openErrorDialog(this.dialog, error)
          )
        }
      }
    )
  }

  hasRight(roles: string[]):boolean{
    let right: boolean = false;

    for(let i = 0; i < roles.length; i++){
      if(roles[i] === this.appConfig.getUserConnected().role) right = true;
    }

    return right;
  }
}
