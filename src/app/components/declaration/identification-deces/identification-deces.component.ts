import { Component, OnInit } from '@angular/core';
import { Personne } from 'src/app/models/personne';
import { MatDialog } from '@angular/material/dialog';
import { DeclarationService } from 'src/app/services/declaration.service';
import { Router } from '@angular/router';
import { Utility } from 'src/app/utilities/utility';

@Component({
  selector: 'app-identification-deces',
  templateUrl: './identification-deces.component.html',
  styleUrls: ['./identification-deces.component.css']
})
export class IdentificationDecesComponent implements OnInit {

  beneficiaire: Personne;
  pere: Personne;
  mere: Personne;

  data = {
    pere: undefined,
    mere: undefined,
    beneficiaire: undefined,
    lieuDeces: undefined,
    dateDeces: undefined,
    circonstance: undefined,
    etatCivil: 'CELIBATAIRE'
  }

  constructor(
    private dialog: MatDialog,
    private service: DeclarationService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  openIdentificationDialog(type: string): void{
    Utility.openIdentificationDialog(this.dialog).afterClosed().subscribe(
      (response: Personne) =>{
        if(response){
          switch(type){
            case 'pere':
              this.pere = response;
              this.data.pere = response.id;
              break;
            case 'mere':
              this.mere = response;
              this.data.mere = response.id;
              break;
            case 'beneficiaire':
              this.beneficiaire = response
              this.data.beneficiaire = response.id;
          }
        }
      }
    )
  }

  next(): void{
    if(!this.data.beneficiaire){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner l\'identité de l\'enfant');
      return;
    }
    if(!this.data.pere){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner l\'identité du père');
      return;
    }
    if(!this.data.mere){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner l\'identité de la mère');
      return;
    }

    if(!this.data.dateDeces){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner la date du deces');
      return;
    }
    if(!this.data.lieuDeces){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner le lieu de deces');
      return;
    }

    this.service.pushActe(this.data);

    this.router.navigate(['/main/declaration/new/archiver'])
  }

}
