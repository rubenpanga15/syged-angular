import { Component, OnInit, Input } from '@angular/core';
import { Declaration } from 'src/app/models/declaration';
import { MatDialog } from '@angular/material/dialog';
import { ConsultDeclarationDialogComponent } from 'src/app/dialogs/consult-declaration-dialog/consult-declaration-dialog.component';

@Component({
  selector: 'app-table-declarations',
  templateUrl: './table-declarations.component.html',
  styleUrls: ['./table-declarations.component.css']
})
export class TableDeclarationsComponent implements OnInit {

  motclef: string = ''
  @Input() declarations: Declaration[]

  data: Declaration[]

  constructor(
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
  }

  filterData(): Declaration[]{
    return this.declarations.filter(declaration => {

      return this.motclef === '' || (declaration.numero.includes(this.motclef) || declaration.beneficiaire.nom.includes(this.motclef) || declaration.beneficiaire.prenom.includes(this.motclef)) && !declaration.fkPrepose
    }
    )
  }

  onFilterChange(): void{
    
    this.filterData();
  }

  openDialog(datum: Declaration): void{
    this.dialog.open(ConsultDeclarationDialogComponent, 
      {
        data: datum,
        width: '90%', height: '90%'
      }
    )
  }

}
