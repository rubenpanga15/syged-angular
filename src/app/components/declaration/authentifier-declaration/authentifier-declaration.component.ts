import { Component, OnInit } from '@angular/core';
import { Declaration } from 'src/app/models/declaration';
import { AppConfigService } from 'src/app/services/app-config.service';
import { MatDialog } from '@angular/material/dialog';
import { DeclarationService } from 'src/app/services/declaration.service';
import { Utility } from 'src/app/utilities/utility';

@Component({
  selector: 'app-authentifier-declaration',
  templateUrl: './authentifier-declaration.component.html',
  styleUrls: ['./authentifier-declaration.component.css']
})
export class AuthentifierDeclarationComponent implements OnInit {

  data = {
    dateDebut: undefined,
    dateFin: undefined,
    type: '',
    fkCentre: undefined,
    username: undefined,
    token: undefined
  }

  declarations: Declaration[];

  constructor(
    private appConfig: AppConfigService,
    private dialog: MatDialog,
    private service: DeclarationService
  ) { 
    this.data.fkCentre = appConfig.getUserConnected().fkCentre;
    this.data.username = appConfig.getUserConnected().username;
    this.data.token = appConfig.getUserConnected().token;
  }

  ngOnInit(): void {
    
  }

  onFilter(): void{
    this.service.recupererDossierParPeriode(JSON.stringify(this.data))
    .then(
      result => {
        this.declarations = result.response.filter(
          value => {
            let isValid = false;
            if(value.fkPrepose && value.avis === 'VALIDE') {
              switch(this.data.type){
                case '': isValid = true; break;
                case 'AUTHENTIFIE' : 
                  if(value.fkOec) isValid = true; break;
                case 'NON-AUTHENTIFIE' :
                  if(!value.fkOec) isValid = true; break;
              }
            }
            console.log(isValid)
            return isValid;
          }
        )
      }
    )
    .catch(
      error => Utility.openErrorDialog(this.dialog, error)
    )
  }

}
