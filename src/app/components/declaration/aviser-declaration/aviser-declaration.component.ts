import { Component, OnInit } from '@angular/core';
import { AppConfigService } from 'src/app/services/app-config.service';
import { MatDialog } from '@angular/material/dialog';
import { DeclarationService } from 'src/app/services/declaration.service';
import { Declaration } from 'src/app/models/declaration';
import { Utility } from 'src/app/utilities/utility';

@Component({
  selector: 'app-aviser-declaration',
  templateUrl: './aviser-declaration.component.html',
  styleUrls: ['./aviser-declaration.component.css']
})
export class AviserDeclarationComponent implements OnInit {

  data = {
    dateDebut: undefined,
    dateFin: undefined,
    type: '',
    fkCentre: undefined,
    username: undefined,
    token: undefined
  }

  declarations: Declaration[];

  constructor(
    private appConfig: AppConfigService,
    private dialog: MatDialog,
    private service: DeclarationService
  ) { 
    this.data.fkCentre = appConfig.getUserConnected().fkCentre;
    this.data.username = appConfig.getUserConnected().username;
    this.data.token = appConfig.getUserConnected().token;
  }

  ngOnInit(): void {
    
  }

  onFilter(): void{
    this.service.recupererDossierParPeriode(JSON.stringify(this.data))
    .then(
      result => {
        this.declarations = result.response.filter(
          value => {
            let isValid = false;
              switch(this.data.type){
                case '': isValid = true; break;
                case 'AVISE' : 
                  if(value.fkPrepose) isValid = true; break;
                case 'NON-AVISE' :
                  if(!value.fkPrepose) isValid = true; break;
              }
            return isValid;
          }
        )
      }
    )
    .catch(
      error => Utility.openErrorDialog(this.dialog, error)
    )
  }

}
