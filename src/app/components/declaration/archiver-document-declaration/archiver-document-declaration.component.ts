import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { DocumentProcedure } from 'src/app/models/document-procedure';
import { TypeDocument } from 'src/app/models/type-document';
import { AppConfigService } from 'src/app/services/app-config.service';
import { DeclarationService } from 'src/app/services/declaration.service';
import { Utility } from 'src/app/utilities/utility';
import { ResumeDeclarationComponent } from '../../resume-declaration/resume-declaration.component';

@Component({
  selector: 'app-archiver-document-declaration',
  templateUrl: './archiver-document-declaration.component.html',
  styleUrls: ['./archiver-document-declaration.component.css']
})
export class ArchiverDocumentDeclarationComponent implements OnInit {

  typeDocuments: DocumentProcedure[];

  documentRequests = [];
  uploadFileFordeclarationRequestItem = []

  constructor(
    private appConfig: AppConfigService,
    private dialog: MatDialog,
    private service: DeclarationService,
    private router: Router
  ) { }

  data = {
    username: this.appConfig.getUserConnected().username,
    token:  this.appConfig.getUserConnected().token,
    type: this.service.getDeclarationRequest().declaration.type
  }

  ngOnInit(): void {
    console.log(this.data);
    this.service.getDocumentsAArchiver(JSON.stringify(this.data))
    .then(
      result =>{
        this.typeDocuments = result.response
        this.documentRequests = result.response.map( e =>{
          return {
            description: e.typeDocument.description,
            username: this.appConfig.getUserConnected().username,
            token:  this.appConfig.getUserConnected().token,
            typeDocument: e.fkTypeDocument,
            isObligatoire: e.isObligatoire
          }
        })

        this.uploadFileFordeclarationRequestItem = result.response.map( e =>{
          return {
            numeroDeclaration :undefined,
            typeDocument: e.fkTypeDocument,
            files: []
          }
        }) 
      }
    )
    .catch(
      error => Utility.openErrorDialog(this.dialog, error)
    )
  }

  next(): void{
    this.service.pushData({documents: this.documentRequests});

    Utility.openConfirmDialog(this.dialog, 'Êtes-vous sure de vouloir enregistrer cette declaration?').afterClosed()
    .subscribe(
      response => {
        if(response === 'OK'){
          this.service.saveDeclaration().then(
            result =>{

              this.updateFileForDeclarstionRequestItem(result.response);
              if(this.uploadFileFordeclarationRequestItem.length > 0){
                this.service.uploadFileForDeclaration(JSON.stringify(
                  {
                    username: this.appConfig.getUserConnected().username,
                    token:  this.appConfig.getUserConnected().token,
                    files: this.uploadFileFordeclarationRequestItem
                  }
                )).catch(
                  error => Utility.openErrorDialog(this.dialog, error)
                )
              }

              this.dialog.open(ResumeDeclarationComponent, 
                {
                  data: result.response,
                  width: '90%', height: '90%'
                }).afterClosed().subscribe(
                  res => this.router.navigate(['/main/declaration/new'])
                )
              
            }
          ).catch(
           error =>  {
            Utility.openErrorDialog(this.dialog, error);
            //this.service.recupererDossier()
           }
          )
        }
      }
    )
  }

  addFile(i: number): void{
    Utility.openUploadFileDialog(this.dialog).afterClosed().subscribe(
      (response: string) => {
        if(response)
          this.uploadFileFordeclarationRequestItem[i].files.push(response);
        console.log(this.uploadFileFordeclarationRequestItem);
      }
    )
  }

  updateFileForDeclarstionRequestItem(numero: string): void{
    for(let i = 0; i < this.uploadFileFordeclarationRequestItem.length; i++){
      this.uploadFileFordeclarationRequestItem[i].numeroDeclaration = numero
    }
  }
}
