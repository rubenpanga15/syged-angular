import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { InformationDialogComponent } from 'src/app/dialogs/information-dialog/information-dialog.component';
import { Personne } from 'src/app/models/personne';
import { Procedure } from 'src/app/models/procedure';
import { DeclarationService } from 'src/app/services/declaration.service';
import { Utility } from 'src/app/utilities/utility';
import { AppConfigService } from 'src/app/services/app-config.service';
import { ConfigurationService } from 'src/app/services/configuration.service';

@Component({
  selector: 'app-identifier-declarant',
  templateUrl: './identifier-declarant.component.html',
  styleUrls: ['./identifier-declarant.component.css']
})
export class IdentifierDeclarantComponent implements OnInit {

  declarant: Personne;
  beneficiaire: Personne;
  interprete: Personne;

  isBeneficiaire: boolean = true;
  pro: Procedure = new Procedure();
  procedures: Procedure[];

  data = {
    requerant: undefined,
    type: '',
    fkProcuration: undefined,
    preuvePaiement: undefined,
    interprete: undefined,
    langue: undefined,
    fkCentre: undefined

  }

  constructor(
    private dialog: MatDialog,
    private router: Router,
    private service: DeclarationService,
    private appConfig: AppConfigService,
    private configService: ConfigurationService
  ) {
    service.clearData();
    this.data.fkCentre = appConfig.getUserConnected().fkCentre;
   }

  ngOnInit(): void {
    this.procedures = []
    this.pro.id = 1;
    this.pro.description= "ACTE DE NAISSANCE";
    this.pro.type = "ACTE DE NAISSANCE"
    this.pro.isPayant = false;
    this.procedures.push(this.pro);

    this.configService.getProcedures().then(
      result => this.procedures = result.response
    ).catch(
      error => Utility.openErrorDialog(this.dialog, error)
    )
  }

  openIdentificationDialog(type: string): void{
    Utility.openIdentificationDialog(this.dialog).afterClosed().subscribe(
      (response: Personne) =>{
        if(response){
          if(type === 'interprete'){
            this.interprete = response;
            this.data.interprete = response.id
          }
          if(type === 'declarant'){
            this.declarant = response;
            this.data.requerant = response.id
          }
          
        }else{
          this.data.requerant = undefined
        }
      }
    )
  }

  isPayant(): boolean{
    if(!this.procedures.find(e => e.type === this.data.type))
      return false;
    return this.procedures.find(e => e.type === this.data.type).isPayant;
  }

  next(): void{
    
    
    if(!this.data.requerant){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner le requerant');
      return;
    }

    if(!this.isBeneficiaire && (!this.data.fkProcuration || this.data.fkProcuration.length <= 0)){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner le numéro de la procuration');
      return;
    }

    if(!this.data.langue || this.data.langue.length <= 0){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner la langue de rédaction');
      return;
    }

    if(this.data.type.length <= 0){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner le type d\'acte');
      return;
    }

    if(this.isPayant() && !this.data.preuvePaiement){
      Utility.openInfoDialog(this.dialog,'Veuillez renseigner la preuve de paiement');
      return;
    }

    this.service.pushData(this.data);

    this.navigate(this.data.type);
    
    
  }

  private navigate(acte: string): void{
    switch(acte){
      case 'ACTE DE NAISSANCE':
        this.router.navigate(['/main/declaration/new/naissance']);
        break;
      case 'ACTE DE DECES':
        this.router.navigate(['/main/declaration/new/deces']);
        break;
      case 'ACTE DE MARIAGE':
        this.router.navigate(['/main/declaration/new/mariage']);
        break;
    }
  }

}
