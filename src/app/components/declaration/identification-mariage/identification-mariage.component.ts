import { Component, OnInit } from '@angular/core';
import { Personne } from 'src/app/models/personne';
import { MatDialog } from '@angular/material/dialog';
import { DeclarationService } from 'src/app/services/declaration.service';
import { Router } from '@angular/router';
import { Utility } from 'src/app/utilities/utility';

@Component({
  selector: 'app-identification-mariage',
  templateUrl: './identification-mariage.component.html',
  styleUrls: ['./identification-mariage.component.css']
})
export class IdentificationMariageComponent implements OnInit {

  
  beneficiaire: Personne;
  pereEpoux: Personne;
  mereEpoux: Personne;
  pereEpouse: Personne;
  mereEpouse: Personne;
  epoux: Personne;
  epouse: Personne;

  data = {
    pereEpoux: undefined,
    mereEpoux: undefined,
    pereEpouse: undefined,
    mereEpouse: undefined,
    beneficiaire: undefined,
    lieuPublicite: undefined,
    dateMariage: undefined,
    regimeMatrimonial: undefined,
    epoux: undefined,
    epouse: undefined
  }

  constructor(
    private dialog: MatDialog,
    private service: DeclarationService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  openIdentificationDialog(type: string): void{
    Utility.openIdentificationDialog(this.dialog).afterClosed().subscribe(
      (response: Personne) =>{
        if(response){
          switch(type){
            case 'epoux':
              this.epoux = response;
              this.data.epoux = response.id;
              this.data.beneficiaire = response.id;
              break;
            case 'epouse':
              this.epouse = response;
              this.data.epouse = response.id;
              break;
            case 'pereEpoux':
              this.pereEpoux = response;
              this.data.pereEpoux = response.id;
              break;
            case 'mereEpoux':
              this.mereEpoux = response;
              this.data.mereEpoux = response.id;
              break;
            case 'pereEpouse':
              this.pereEpouse = response;
              this.data.pereEpouse = response.id;
              break;
            case 'mereEpouse':
              this.mereEpouse = response;
              this.data.mereEpouse = response.id;
              break;
            case 'beneficiaire':
              this.beneficiaire = response
              this.data.beneficiaire = response.id;
          }
        }
      }
    )
  }

  next(): void{
    if(!this.data.epoux){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner le mari');
      return;
    }
    if(!this.data.epouse){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner la femme');
      return;
    }
    if(!this.data.pereEpoux){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner du père du mari ');
      return;
    }
    if(!this.data.mereEpoux){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner la mère du mari');
      return;
    }
    if(!this.data.pereEpouse){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner du père de l\'epouse');
      return;
    }
    if(!this.data.mereEpouse){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner l\'identité de la mère de l\'epouse');
      return;
    }

    if(!this.data.dateMariage){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner la date du mariage');
      return;
    }
    if(!this.data.lieuPublicite){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner le lieu de publicité');
      return;
    }
    if(!this.data.regimeMatrimonial){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner le regime matrimonial');
      return;
    }

    this.service.pushActe(this.data);

    this.router.navigate(['/main/declaration/new/archiver'])
  }

}
