import { Component, OnInit } from '@angular/core';
import { AppConfigService } from 'src/app/services/app-config.service';

@Component({
  selector: 'app-main-declaration',
  templateUrl: './main-declaration.component.html',
  styleUrls: ['./main-declaration.component.css']
})
export class MainDeclarationComponent implements OnInit {

  constructor(
    private appConfig: AppConfigService
  ) { }

  ngOnInit(): void {
  }

  hasRight(roles: string[]):boolean{
    let right: boolean = false;

    for(let i = 0; i < roles.length; i++){
      if(roles[i] === this.appConfig.getUserConnected().role) right = true;
    }

    return right;
  }

}
