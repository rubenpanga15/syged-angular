import { Component, Inject, Input, OnInit } from '@angular/core';
import { Personne } from 'src/app/models/personne';

@Component({
  selector: 'app-view-personne-box',
  templateUrl: './view-personne-box.component.html',
  styleUrls: ['./view-personne-box.component.css']
})
export class ViewPersonneBoxComponent implements OnInit {

  constructor() { }

  @Input() personne: Personne;
  @Input() title: string

  ngOnInit(): void {
  }

}
