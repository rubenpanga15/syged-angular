import { Component, Inject, inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Declaration } from 'src/app/models/declaration';
import { AppConfigService } from 'src/app/services/app-config.service';
import { DeclarationService } from 'src/app/services/declaration.service';
import { Utility } from 'src/app/utilities/utility';

@Component({
  selector: 'app-resume-declaration',
  templateUrl: './resume-declaration.component.html',
  styleUrls: ['./resume-declaration.component.css']
})
export class ResumeDeclarationComponent implements OnInit {

  declaration: Declaration;
  numero: string = '';

  constructor(
    private service: DeclarationService,
    private dialog: MatDialog,
    private appConfig: AppConfigService,
    private dialogRef: MatDialogRef<ResumeDeclarationComponent>,
    @Inject(MAT_DIALOG_DATA) data: string
  ) { 
    this.numero = data
  }

  ngOnInit(): void {
    this.service.recupererDossier(this.numero)
    .then(
      result => this.declaration = result.response
    ).catch(
      error => {
        Utility.openInfoDialog(this.dialog, error);
        this.dialogRef.close();
      }
    )
  }


}
