import { Component, OnInit } from '@angular/core';
import { UserAdapter } from 'src/app/models/utilities/user-adapter';
import { AppConfigService } from 'src/app/services/app-config.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  user: UserAdapter

  constructor(
    private appConfig: AppConfigService
  ) { 
    this.user = this.appConfig.getUserConnected();
  }

  ngOnInit(): void {
  }

}
