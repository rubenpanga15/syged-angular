import { Component, Input, OnInit } from '@angular/core';
import { ActeDeces } from 'src/app/models/acte-deces';

@Component({
  selector: 'app-acte-deces-view-box',
  templateUrl: './acte-deces-view-box.component.html',
  styleUrls: ['./acte-deces-view-box.component.css']
})
export class ActeDecesViewBoxComponent implements OnInit {

  @Input() acte: ActeDeces

  constructor() { }

  ngOnInit(): void {
  }

}
