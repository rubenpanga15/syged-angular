import { Component, Input, OnInit } from '@angular/core';
import { ActeMariage } from 'src/app/models/acte-mariage';

@Component({
  selector: 'app-acte-mariage-view-box',
  templateUrl: './acte-mariage-view-box.component.html',
  styleUrls: ['./acte-mariage-view-box.component.css']
})
export class ActeMariageViewBoxComponent implements OnInit {

  @Input() acte: ActeMariage

  constructor() { }

  ngOnInit(): void {
  }

}
