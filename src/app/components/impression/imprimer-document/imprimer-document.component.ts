import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-imprimer-document',
  templateUrl: './imprimer-document.component.html',
  styleUrls: ['./imprimer-document.component.css']
})
export class ImprimerDocumentComponent implements OnInit {

  data = {
    dateDebut: undefined,
    dateFin: undefined,
    type: 'ACTE DE NAISSANCE',
    fkCentre: undefined,
    username: undefined,
    token: undefined,
    imprimer: false
  }

  constructor() { }

  ngOnInit(): void {
  }

  onFilter(): void{

  }

}
