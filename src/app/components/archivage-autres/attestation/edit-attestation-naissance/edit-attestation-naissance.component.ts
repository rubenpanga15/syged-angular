import { Component, OnInit } from '@angular/core';
import { AttestationNaissance } from 'src/app/models/attestation-naissance';
import { Utility } from 'src/app/utilities/utility';
import { Personne } from 'src/app/models/personne';
import { ConsultProcurationDialogComponent } from 'src/app/dialogs/consult-procuration-dialog/consult-procuration-dialog.component';
import { AppConfigService } from 'src/app/services/app-config.service';
import { MatDialog } from '@angular/material/dialog';
import { ArchivageAutreService } from 'src/app/services/archivage-autre.service';
import { ConsultAttestationDialogComponent } from 'src/app/dialogs/consult-attestation-dialog/consult-attestation-dialog.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-attestation-naissance',
  templateUrl: './edit-attestation-naissance.component.html',
  styleUrls: ['./edit-attestation-naissance.component.css']
})
export class EditAttestationNaissanceComponent implements OnInit {

  attestation: AttestationNaissance = new AttestationNaissance();

  constructor(
    private appConfig: AppConfigService,
    private dialog: MatDialog,
    private service: ArchivageAutreService,
    private router: Router
  ) { 
    this.attestation.fkCentre = appConfig.getUserConnected().fkCentre
  }

  ngOnInit(): void {
  }

  openIdentificationDialog(type: string): void{
    Utility.openIdentificationDialog(this.dialog).afterClosed().subscribe(
      (response: Personne) =>{
        if(response){
          switch(type){
            case 'pere':
              this.attestation.pere = response;
              this.attestation.fkPere = response.id;
              break;
            case 'mere':
              this.attestation.mere = response;
              this.attestation.fkMere = response.id;
              break;
            case 'enfant':
              this.attestation.enfant = response
              this.attestation.fkEnfant = response.id;
              break;
            
          }
        }
      }
    )
  }

  valider(): void{
    if(!this.attestation.enfant){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner l\'identité de l\'enfant');
      return;
    }
    if(!this.attestation.pere){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner l\'identité du père');
      return;
    }
    if(!this.attestation.mere){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner l\'identité de la mère');
      return;
    }
    if(!this.attestation.dateNaissance){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner la date de naissance');
      return;
    }
    if(!this.attestation.lieuNaissance){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner le lieu de naissance');
      return;
    }

    const data = {
      username: this.appConfig.getUserConnected().username,
      token: this.appConfig.getUserConnected().token,
      attestation: this.attestation,
      type: 'ATTESTATION DE NAISSANCE'
    }

    Utility.openConfirmDialog(this.dialog, 'Êtes-vous sûr(e) de vouloir enregistrer cette attestion')
    .afterClosed()
    .subscribe(
      res =>{
        if(res === 'OK'){
          this.service.saveAttestation(JSON.stringify(data))
          .then(
            result => {
              //Utility.openSuccessDialog(this.dialog, result.error.errorDescription);
              this.attestation.numero = result.response;
              this.dialog.open(ConsultAttestationDialogComponent,
                {
                  width: '90%', height: '90%',
                  data: {
                    data: this.attestation,
                    type: 'ATTESTATION DE NAISSANCE'
                  }
                })
                .afterClosed().subscribe(
                  res => this.router.navigate(['/main/archivage-autres/attestation/edit'])
                )
            }
          )
          .catch(
            error => Utility.openErrorDialog(this.dialog, error)
          )
        }
      }
    )

  }

}
