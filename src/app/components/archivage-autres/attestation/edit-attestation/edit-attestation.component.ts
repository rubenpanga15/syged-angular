import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-attestation',
  templateUrl: './edit-attestation.component.html',
  styleUrls: ['./edit-attestation.component.css']
})
export class EditAttestationComponent implements OnInit {

  data:string = ''

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  onSelectType(): void{
    switch(this.data){
      case '':
        this.router.navigate(['/main/archivage-autres/attestation/edit'], {replaceUrl: true});
        break;
      case 'ATTESTATION DE NAISSANCE':
        this.router.navigate(['/main/archivage-autres/attestation/edit/attestation-naissance'], {replaceUrl: true});
        break;
      case 'ATTESTATION DE DECES':
        this.router.navigate(['/main/archivage-autres/attestation/edit/attestation-deces'], {replaceUrl: true});
        break;
      case 'ATTESTATION DE MARIAGE':
        this.router.navigate(['/main/archivage-autres/attestation/edit/attestation-mariage'], {replaceUrl: true});
        break;
      case 'ATTESTATION SPECIALE':
        this.router.navigate(['/main/archivage-autres/attestation/edit/attestation-speciale'], {replaceUrl: true});
        break;
    }
  }

}
