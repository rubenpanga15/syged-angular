import { Component, OnInit, Input } from '@angular/core';
import { ProcurationDeces } from 'src/app/models/procuration-deces';

@Component({
  selector: 'app-view-procuration-deces',
  templateUrl: './view-procuration-deces.component.html',
  styleUrls: ['./view-procuration-deces.component.css']
})
export class ViewProcurationDecesComponent implements OnInit {

  @Input() procuration: ProcurationDeces

  constructor() { }

  ngOnInit(): void {
  }

}
