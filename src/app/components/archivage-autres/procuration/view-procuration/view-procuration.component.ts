import { Component, OnInit, Input } from '@angular/core';
import { ProcurationNaissance } from 'src/app/models/procuration-naissance';

@Component({
  selector: 'app-view-procuration',
  templateUrl: './view-procuration.component.html',
  styleUrls: ['./view-procuration.component.css']
})
export class ViewProcurationComponent implements OnInit {

  @Input() procuration: ProcurationNaissance;

  constructor() { }

  ngOnInit(): void {
  }

}
