import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-procuration',
  templateUrl: './edit-procuration.component.html',
  styleUrls: ['./edit-procuration.component.css']
})
export class EditProcurationComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  data:string = ''

  ngOnInit(): void {
  }

  onSelectType(): void{
    switch(this.data){
      case '':
        this.router.navigate(['/main/archivage-autres/procuration/edit'], {replaceUrl: true});
        break;
      case 'PROCURATION DE NAISSANCE':
        this.router.navigate(['/main/archivage-autres/procuration/edit/procuration-naissance'], {replaceUrl: true});
        break;
      case 'PROCURATION DE DECES':
        this.router.navigate(['/main/archivage-autres/procuration/edit/procuration-deces'], {replaceUrl: true});
        break;
      case 'PROCURATION DE SPECIALE':
        this.router.navigate(['/main/archivage-autres/procuration/edit/procuration-speciale'], {replaceUrl: true});
        break;
    }
  }

}
