import { Component, OnInit } from '@angular/core';
import { Personne } from 'src/app/models/personne';
import { Utility } from 'src/app/utilities/utility';
import { AppConfigService } from 'src/app/services/app-config.service';
import { MatDialog } from '@angular/material/dialog';
import { ProcurationNaissance } from 'src/app/models/procuration-naissance';
import { ArchivageAutreService } from 'src/app/services/archivage-autre.service';
import { ConsultProcurationDialogComponent } from 'src/app/dialogs/consult-procuration-dialog/consult-procuration-dialog.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-procuration-naissance',
  templateUrl: './edit-procuration-naissance.component.html',
  styleUrls: ['./edit-procuration-naissance.component.css']
})
export class EditProcurationNaissanceComponent implements OnInit {

  beneficiaire: Personne;
  pere: Personne;
  mere: Personne;
  interesse: Personne

  procuration: ProcurationNaissance = new ProcurationNaissance();

  constructor(
    private appConfig: AppConfigService,
    private dialog: MatDialog,
    private service: ArchivageAutreService,
    private router: Router
  ) { 
    this.procuration.agentCreat = appConfig.getUserConnected().id;
    this.procuration.fkCentre = appConfig.getUserConnected().fkCentre;
  }

  ngOnInit(): void {
  }

  openIdentificationDialog(type: string): void{
    Utility.openIdentificationDialog(this.dialog).afterClosed().subscribe(
      (response: Personne) =>{
        if(response){
          switch(type){
            case 'pere':
              this.procuration.pere = response;
              this.procuration.fkPere = response.id;
              break;
            case 'mere':
              this.procuration.mere = response;
              this.procuration.fkMere = response.id;
              break;
            case 'enfant':
              this.procuration.enfant = response
              this.procuration.fkEnfant = response.id;
              break
            case 'interesse':
              this.procuration.interessePersonne = response;
              this.procuration.interesse = response.id;
              break;
            case 'mandataire':
              this.procuration.mandatairePersonne = response;
              this.procuration.mandataire = response.id;
              break;
            case 'personneContact':
              this.procuration.personneContactPersonne = response;
              this.procuration.personneContact = response.id;
              break;
            
          }
        }
      }
    )
  }

  valider(): void{
    if(!this.procuration.enfant){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner l\'identité de l\'enfant');
      return;
    }
    if(!this.procuration.pere){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner l\'identité du père');
      return;
    }
    if(!this.procuration.mere){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner l\'identité de la mère');
      return;
    }
    if(!this.procuration.mandatairePersonne){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner le mandataire');
      return;
    }
    if(!this.procuration.personneContactPersonne){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner une personne à contacter');
      return;
    }
    if(!this.procuration.interessePersonne){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner une personne à contacter');
      return;
    }
    if(!this.procuration.situationMatrimoniale){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner la situation matrimoniale');
      return;
    }

    const data = {
      username: this.appConfig.getUserConnected().username,
      token: this.appConfig.getUserConnected().token,
      procuration: this.procuration,
      type: 'PROCURATION DE NAISSANCE'
    }

    Utility.openConfirmDialog(this.dialog, 'Êtes-vous sûr(e) de vouloir enregistrer cette procuration?')
    .afterClosed()
    .subscribe(
      result => {
        if(result === 'OK'){
          this.service.saveProcuration(JSON.stringify(data))
          .then(
            result => {
              this.procuration.numero = result.response;
              this.dialog.open(ConsultProcurationDialogComponent,
                {
                  width: '90%', height: '90%',
                  data: {
                    procuration: this.procuration,
                    type: 'PROCURATION DE NAISSANCE'
                  }
                }).afterClosed().subscribe(
                  res => this.router.navigate(['/main/archivage-autres/procuration/edit/procuration-naissance'])
                )
            }
          )
          .catch(
            error => Utility.openErrorDialog(this.dialog, error)
          )
        }
      }
    )

  }

}
