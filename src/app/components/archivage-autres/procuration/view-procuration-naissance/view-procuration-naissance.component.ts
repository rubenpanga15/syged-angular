import { Component, OnInit, Input } from '@angular/core';
import { ProcurationNaissance } from 'src/app/models/procuration-naissance';

@Component({
  selector: 'app-view-procuration-naissance',
  templateUrl: './view-procuration-naissance.component.html',
  styleUrls: ['./view-procuration-naissance.component.css']
})
export class ViewProcurationNaissanceComponent implements OnInit {

  @Input() procuration: ProcurationNaissance
  constructor() { }

  ngOnInit(): void {
  }

}
