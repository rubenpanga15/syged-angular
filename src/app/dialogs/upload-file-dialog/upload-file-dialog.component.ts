import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-upload-file-dialog',
  templateUrl: './upload-file-dialog.component.html',
  styleUrls: ['./upload-file-dialog.component.css']
})
export class UploadFileDialogComponent implements OnInit {

  ImageBaseData:string | ArrayBuffer=null;

  constructor(
    private dialogRef: MatDialogRef<UploadFileDialogComponent, string>
  ) { }

  ngOnInit(): void {
  }

  handleFileInput(files: FileList) {
    let me = this;
    let file = files[0];
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
      me.ImageBaseData=reader.result;
    };
    reader.onerror = function (error) {
      console.log('Error: ', error);
    };
 }

 btnUpload(){
    
  if(this.ImageBaseData){
    this.dialogRef.close(this.ImageBaseData.toString())
  }else{
    this.dialogRef.close();
  }
}

}
