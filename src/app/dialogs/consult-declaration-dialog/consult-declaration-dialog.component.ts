import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Declaration } from 'src/app/models/declaration';

@Component({
  selector: 'app-consult-declaration-dialog',
  templateUrl: './consult-declaration-dialog.component.html',
  styleUrls: ['./consult-declaration-dialog.component.css']
})
export class ConsultDeclarationDialogComponent implements OnInit {

  declaration: Declaration

  constructor(
    private dialogRef: MatDialogRef<ConsultDeclarationDialogComponent>,
    private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) data: Declaration
  ) { 
    this.declaration = data;
  }

  ngOnInit(): void {
    console.log('CONSULT',this.declaration)
  }

  aviserDeclaration(): void{
    
  }

}
