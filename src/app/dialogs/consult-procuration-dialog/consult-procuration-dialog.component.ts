import { Component, OnInit, Inject } from '@angular/core';
import { AppConfigService } from 'src/app/services/app-config.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-consult-procuration-dialog',
  templateUrl: './consult-procuration-dialog.component.html',
  styleUrls: ['./consult-procuration-dialog.component.css']
})
export class ConsultProcurationDialogComponent implements OnInit {

  procuration: any;
  type: string

  constructor(
    private appConfig: AppConfigService,
    private dialogRef: MatDialogRef<ConsultProcurationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data: any
  ) { 
    this.procuration = data.procuration
    this.type = data.type
  }

  ngOnInit(): void {
  }

}
