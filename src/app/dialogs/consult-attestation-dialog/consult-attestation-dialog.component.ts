import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-consult-attestation-dialog',
  templateUrl: './consult-attestation-dialog.component.html',
  styleUrls: ['./consult-attestation-dialog.component.css']
})
export class ConsultAttestationDialogComponent implements OnInit {

  attestation: any
  type:string;

  constructor(
    private dialogRef: MatDialogRef<ConsultAttestationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data: any
  ) { 
    this.attestation = data.data;
    this.type = data.type
  }

  ngOnInit(): void {
  }

}
