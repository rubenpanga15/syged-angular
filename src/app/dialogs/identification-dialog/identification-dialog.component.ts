import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Personne } from 'src/app/models/personne';
import { IdentificationService } from 'src/app/services/identification.service';
import { InformationDialogComponent } from '../information-dialog/information-dialog.component';
import { SavePersonneComponent } from '../save-personne/save-personne.component';
import { AppConfigService } from 'src/app/services/app-config.service';

@Component({
  selector: 'app-identification-dialog',
  templateUrl: './identification-dialog.component.html',
  styleUrls: ['./identification-dialog.component.css']
})
export class IdentificationDialogComponent implements OnInit {

  personnes: Personne[];

  data = {
    username: undefined,
    token: undefined,
    motclef: undefined
  }

  constructor(
    private dialogRef: MatDialogRef<IdentificationDialogComponent>,
    private dialog: MatDialog,
    private service: IdentificationService,
    private appConfig: AppConfigService
  ) { 
    this.data.username = appConfig.getUserConnected().username;
    this.data.token = appConfig.getUserConnected().token;
  }

  ngOnInit(): void {
  }


  onSubmit(): void{
    this.personnes = []
    this.service.searchPersonne(JSON.stringify(this.data)).then(
      result => this.personnes = result.response
    ).catch(error => this.dialog.open(InformationDialogComponent, { data: error}))
  }

  onSelect(personne: Personne): void{
    this.dialogRef.close(personne);
  }

  openSave(): void{
    this.dialog.open(SavePersonneComponent, 
      {
        width: '80%',
        height: '90%',
        disableClose: true,
        backdropClass: 'bg-dark'
      }).afterClosed().subscribe(
      (result: Personne) =>{
        if(result){
          this.personnes = []
          this.personnes.push(result);
        }
      }
    )
  }

}
