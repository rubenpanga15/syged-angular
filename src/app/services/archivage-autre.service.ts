import { Injectable } from '@angular/core';
import { AppConfigService } from './app-config.service';
import { HttpClient } from '@angular/common/http';
import { HttpURLs } from '../utilities/http-urls';
import { HttpDataResponse } from '../utilities/http-data-response';
import { ErrorResponse } from '../utilities/error-response';
import { AppConst } from '../utilities/app-const';

@Injectable({
  providedIn: 'root'
})
export class ArchivageAutreService {

  constructor(
    private appConfig: AppConfigService,
    private httpClient: HttpClient
  ) { }


  saveProcuration(data: string): Promise<HttpDataResponse<string>>{
    this.appConfig.onStartWaiting('Enregistrement en cours...');
    return new Promise<HttpDataResponse<string>>((resolve, reject) => {
      this.httpClient.post(HttpURLs.URL_SAVE_PROCURATION, data)
      .subscribe(
        (result: HttpDataResponse<string>) => {
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result)
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  saveAttestation(data: string): Promise<HttpDataResponse<string>>{
    this.appConfig.onStartWaiting('Enregistrement en cours....');
    return new Promise<HttpDataResponse<string>>((resolve, reject) =>{
      this.httpClient.post(HttpURLs.URL_SAVE_ATTESTATION, data)
      .subscribe(
        (result: HttpDataResponse<string>) => {
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result);
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }
}
