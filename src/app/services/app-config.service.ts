import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { WaitingDialogComponent } from '../dialogs/waiting-dialog/waiting-dialog.component';
import { UserAdapter } from '../models/utilities/user-adapter';

@Injectable({
  providedIn: 'root'
})
export class AppConfigService {

  waiting$: Subject<boolean> = new Subject<boolean>();
  userConnected$: Subject<UserAdapter> = new Subject<UserAdapter>();

  userConnected: UserAdapter;

  private dialogRef: MatDialogRef<WaitingDialogComponent>;

  constructor(
    private dialog: MatDialog,
    
  ) { }

  getUserConnected(): UserAdapter{
    return this.userConnected;
  }

  onConnected(user: UserAdapter): void{
    this.userConnected = user;
    this.userConnected$.next(user);
  }

  onDisconnected(): void{
    this.userConnected = undefined;
    this.userConnected$.next(undefined);
  }

  onStartWaiting(message? : string): void{
    this.dialogRef = this.dialog.open(WaitingDialogComponent, {
      disableClose: true,
      data: message
    });
  }

  onStopWaiting(): void{
    this.dialogRef.close();
  }

  hasRight(roles: string[]):boolean{
    let right: boolean = false;

    for(let i = 0; i < roles.length; i++){
      if(roles[i] === this.getUserConnected().role) right = true;
    }

    return right;
  }


}
