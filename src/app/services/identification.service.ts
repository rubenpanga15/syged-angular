import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Personne } from '../models/personne';
import { AppConst } from '../utilities/app-const';
import { ErrorResponse } from '../utilities/error-response';
import { HttpDataResponse } from '../utilities/http-data-response';
import { HttpURLs } from '../utilities/http-urls';
import { AppConfigService } from './app-config.service';

@Injectable({
  providedIn: 'root'
})
export class IdentificationService {

  constructor(
    private appConfig: AppConfigService,
    private httpClient: HttpClient
  ) { }

  searchPersonne(data: string): Promise<HttpDataResponse<Personne[]>>{
    return new Promise<HttpDataResponse<Personne[]>>((resolve, reject) =>{
      this.appConfig.onStartWaiting();
      this.httpClient.post(HttpURLs.URL_SEARCH_PERSON, data).subscribe(
        (result: HttpDataResponse<Personne[]>) => {
          console.log(result)
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result);
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }
}
