import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { UserAdapter } from '../models/utilities/user-adapter';
import { AppConst } from '../utilities/app-const';
import { ErrorResponse } from '../utilities/error-response';
import { HttpDataResponse } from '../utilities/http-data-response';
import { HttpURLs } from '../utilities/http-urls';
import { AppConfigService } from './app-config.service';

@Injectable({
  providedIn: 'root'
})
export class AuthentificationServiceService {

  constructor(
    private httpClient: HttpClient,
    private appConfig: AppConfigService
  ) { }

  connexion(data: string): Promise<HttpDataResponse<UserAdapter>>{
    return new Promise<HttpDataResponse<UserAdapter>>((resolve, reject) => {
      this.appConfig.onStartWaiting('Authentification en cours...');
      this.httpClient.post(HttpURLs.URL_CONNEXION, data).subscribe(
        (result: HttpDataResponse<UserAdapter>) =>{
          console.log(result)
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result)
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }
}
