import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UploadFileDialogComponent } from '../dialogs/upload-file-dialog/upload-file-dialog.component';
import { Declaration } from '../models/declaration';
import { DocumentProcedure } from '../models/document-procedure';
import { TypeDocument } from '../models/type-document';
import { UploadFileForDeclarationResponse } from '../models/utilities/upload-file-for-declaration-response';
import { AppConst } from '../utilities/app-const';
import { ErrorResponse } from '../utilities/error-response';
import { HttpDataResponse } from '../utilities/http-data-response';
import { HttpURLs } from '../utilities/http-urls';
import { AppConfigService } from './app-config.service';

@Injectable({
  providedIn: 'root'
})
export class DeclarationService {

  declarationRequest = {
    declaration: {},
    acte:{}
  }

  constructor(
    private appConfig: AppConfigService,
    private httpClient: HttpClient
  ) { }

  pushData(data): void{
    this.declarationRequest.declaration = {...this.declarationRequest.declaration, ...data}
    console.log(this.declarationRequest);
  }

  pushActe(data): void{
    console.log(data);
    this.declarationRequest.acte = {...this.declarationRequest.acte, ...data}
  }

  clearData(): void{
    this.declarationRequest.declaration= {}
    this.declarationRequest.acte={}
  }

  getDeclarationRequest(): any{
    return this.declarationRequest
  }

  getDocumentsAArchiver(data: string) :Promise<HttpDataResponse<DocumentProcedure[]>>{
    return new Promise<HttpDataResponse<DocumentProcedure[]>>((resolve, reject) =>{
      this.appConfig.onStartWaiting('Récuperation des documents nécessaires...');
      this.httpClient.post(HttpURLs.URL_GET_DOCUMENT_NECESSAIRE, data).subscribe(
        (result: HttpDataResponse<DocumentProcedure[]>) => {
          console.log(result)
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result)
        },
        error =>{ 
          this.appConfig.onStopWaiting()
          reject(AppConst.NETWORK_ERROR)}
      )
    })
  }

  saveDeclaration(): Promise<HttpDataResponse<string>>{
    return new Promise<HttpDataResponse<string>>((resolve, reject) =>{
      this.appConfig.onStartWaiting('Enregistrement de déclaration...');
      this.declarationRequest = {...this.declarationRequest,...{username: this.appConfig.getUserConnected().username, token:  this.appConfig.getUserConnected().token,}}
      this.httpClient.post(HttpURLs.URL_SAVE_DECLARATION, JSON.stringify(this.declarationRequest)).subscribe(
        (result: HttpDataResponse<string>) => {
          this.appConfig.onStopWaiting();
          console.log(result)
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result)
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR)
        }
      )
    })
  }

  uploadFileForDeclaration(data: string): Promise<HttpDataResponse<UploadFileForDeclarationResponse>>{
    return new Promise<HttpDataResponse<UploadFileForDeclarationResponse>>((resolve, reject) => {
      this.httpClient.post(HttpURLs.URL_UPLOAD_FICHIERS, data).subscribe(
        (result: HttpDataResponse<UploadFileDialogComponent>) => {
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result)
        },
        error => {
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  recupererDossier(data: string): Promise<HttpDataResponse<Declaration>>{
    return new Promise<HttpDataResponse<Declaration>>((resolve, reject) => {
      const datas = {
        username: this.appConfig.getUserConnected().username,
        token:  this.appConfig.getUserConnected().token,
        numeroDossier: data
      }
      this.appConfig.onStartWaiting('Récuperation du dossier en cours...');
      this.httpClient.post(HttpURLs.URL_RECUPERER_DOSSIER, JSON.stringify(datas)).subscribe(
        (result: HttpDataResponse<Declaration>) => {
          this.appConfig.onStopWaiting();
          console.log('RECUPERATION',result);
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result)
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  recupererDossierParPeriode(data: string): Promise<HttpDataResponse<Declaration[]>>{
    return new Promise<HttpDataResponse<Declaration[]>>((resolve, reject) => {
      this.appConfig.onStartWaiting('Récuperation du dossier en cours...');
      this.httpClient.post(HttpURLs.URL_RECUPERER_DOSSIER_PAR_PERIODE, data).subscribe(
        (result: HttpDataResponse<Declaration[]>) => {
          this.appConfig.onStopWaiting();
          console.log(result);
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result)
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  aviserDeclaration(data: string): Promise<HttpDataResponse<boolean>>{
    return new Promise<HttpDataResponse<boolean>>((resolve, reject) =>{
      this.appConfig.onStartWaiting();
      this.httpClient.post(HttpURLs.URL_AVISER_DECLARATION, data)
      .subscribe(
        (result: HttpDataResponse<boolean>) => {
          this.appConfig.onStopWaiting();
          console.log(result);
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result)
        }
      ),
      error => {
        this.appConfig.onStopWaiting();
        reject(AppConst.NETWORK_ERROR);
      }
    })
  }

  authentifierActe(data: string): Promise<HttpDataResponse<boolean>>{
    return new Promise<HttpDataResponse<boolean>>((resolve, reject) =>{
      this.appConfig.onStartWaiting();
      this.httpClient.post(HttpURLs.URL_AUTHENTIFIER_ACTE, data)
      .subscribe(
        (result: HttpDataResponse<boolean>) => {
          this.appConfig.onStopWaiting();
          console.log(result);
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result)
        }
      ),
      error => {
        this.appConfig.onStopWaiting();
        reject(AppConst.NETWORK_ERROR);
      }
    })
  }
}
