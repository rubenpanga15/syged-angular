import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfigService } from './app-config.service';
import { MatDialog } from '@angular/material/dialog';
import { HttpDataResponse } from '../utilities/http-data-response';
import { Procedure } from '../models/procedure';
import { HttpURLs } from '../utilities/http-urls';
import { ErrorResponse } from '../utilities/error-response';
import { AppConst } from '../utilities/app-const';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {

  constructor(
    private httpClient: HttpClient,
    private appConfig: AppConfigService,
    private dialog: MatDialog
  ) { }


  getProcedures(): Promise<HttpDataResponse<Procedure[]>>{
    this.appConfig.onStartWaiting('Chargement des procedures...');
    return new Promise<HttpDataResponse<Procedure[]>>((resolve, reject) =>{
      this.httpClient.post(HttpURLs.URL_GET_PROCEDURES, JSON.stringify(
        {
          username: this.appConfig.getUserConnected().username,
          token: this.appConfig.getUserConnected().token
        }
      ))
      .subscribe(
        (result: HttpDataResponse<Procedure[]>) =>{
          console.log(result)
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result);
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR)
        }
      )
    })
  }
}
