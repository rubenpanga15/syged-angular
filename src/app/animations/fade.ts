import { animate, state, style, transition, trigger } from '@angular/animations';


export const Fade = trigger("fade",
    [
        state("in", style({
            opacity: 0
        })),
        state("out", style({
            opacity: 1
        })),
        state("void", style({
            opacity: 0
        })),
        state("*", style({
            opacity: 1
        })),
        transition("out => in", animate("500ms")),
        transition("in => out", animate("500ms")),
        transition("* <=> void", animate("500ms"))
    ]
)