import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './components/login/login.component';
import { FormsModule } from '@angular/forms';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ErrorDialogComponent } from './dialogs/error-dialog/error-dialog.component';
import { SuccessDialogComponent } from './dialogs/succes-dialog/succes-dialog.component';
import { ConfirmDialogComponent } from './dialogs/confirm-dialog/confirm-dialog.component';
import { InformationDialogComponent } from './dialogs/information-dialog/information-dialog.component';
import { SharedModule } from './modules/shared/shared.module';
import { IdentificationDialogComponent } from './dialogs/identification-dialog/identification-dialog.component';
import { SavePersonneComponent } from './dialogs/save-personne/save-personne.component';
import { MainComponent } from './components/main/main.component';
import { MainDeclarationComponent } from './components/declaration/main-declaration/main-declaration.component';
import { NewdeclarationComponent } from './components/declaration/newdeclaration/newdeclaration.component';
import { WaitingDialogComponent } from './dialogs/waiting-dialog/waiting-dialog.component';
import { IdentifierDeclarantComponent } from './components/declaration/identifier-declarant/identifier-declarant.component';
import { ViewPersonneBoxComponent } from './components/declaration/view-personne-box/view-personne-box.component';
import { IdentificationNaissanceComponent } from './components/declaration/identification-naissance/identification-naissance.component';
import { ArchiverDocumentDeclarationComponent } from './components/declaration/archiver-document-declaration/archiver-document-declaration.component';
import { UploadFileDialogComponent } from './dialogs/upload-file-dialog/upload-file-dialog.component';
import { ConsultDeclarationDialogComponent } from './dialogs/consult-declaration-dialog/consult-declaration-dialog.component';
import { DeclarationBoxComponent } from './components/declaration-box/declaration-box.component';
import { ResumeDeclarationComponent } from './components/resume-declaration/resume-declaration.component';
import { ActeNaissanceViewBoxComponent } from './components/acte-naissance-view-box/acte-naissance-view-box.component';
import { ActeDecesViewBoxComponent } from './components/acte-deces-view-box/acte-deces-view-box.component';
import { ActeMariageViewBoxComponent } from './components/acte-mariage-view-box/acte-mariage-view-box.component';
import { AviserDeclarationComponent } from './components/declaration/aviser-declaration/aviser-declaration.component';
import { TableDeclarationsComponent } from './components/declaration/table-declarations/table-declarations.component';
import { AuthentifierDeclarationComponent } from './components/declaration/authentifier-declaration/authentifier-declaration.component';
import { MainImpressionComponent } from './components/impression/main-impression/main-impression.component';
import { ImprimerDocumentComponent } from './components/impression/imprimer-document/imprimer-document.component';
import { MainArchivageAutresComponent } from './components/archivage-autres/main-archivage-autres/main-archivage-autres.component';
import { MainProcurationComponent } from './components/archivage-autres/procuration/main-procuration/main-procuration.component';
import { EditProcurationComponent } from './components/archivage-autres/procuration/edit-procuration/edit-procuration.component';
import { ListProcurationComponent } from './components/archivage-autres/procuration/list-procuration/list-procuration.component';
import { EditProcurationNaissanceComponent } from './components/archivage-autres/procuration/edit-procuration-naissance/edit-procuration-naissance.component';
import { EditProcurationDecesComponent } from './components/archivage-autres/procuration/edit-procuration-deces/edit-procuration-deces.component';
import { EditProcurationSpecialeComponent } from './components/archivage-autres/procuration/edit-procuration-speciale/edit-procuration-speciale.component';
import { ViewProcurationComponent } from './components/archivage-autres/procuration/view-procuration/view-procuration.component';
import { ViewProcurationNaissanceComponent } from './components/archivage-autres/procuration/view-procuration-naissance/view-procuration-naissance.component';
import { ViewProcurationDecesComponent } from './components/archivage-autres/procuration/view-procuration-deces/view-procuration-deces.component';
import { ConsultProcurationDialogComponent } from './dialogs/consult-procuration-dialog/consult-procuration-dialog.component';
import { MainAttestationComponent } from './components/archivage-autres/attestation/main-attestation/main-attestation.component';
import { EditAttestationComponent } from './components/archivage-autres/attestation/edit-attestation/edit-attestation.component';
import { EditAttestationNaissanceComponent } from './components/archivage-autres/attestation/edit-attestation-naissance/edit-attestation-naissance.component';
import { EditAttestationDecesComponent } from './components/archivage-autres/attestation/edit-attestation-deces/edit-attestation-deces.component';
import { ConsultAttestationDialogComponent } from './dialogs/consult-attestation-dialog/consult-attestation-dialog.component';
import { IdentificationDecesComponent } from './components/declaration/identification-deces/identification-deces.component';
import { IdentificationMariageComponent } from './components/declaration/identification-mariage/identification-mariage.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    ErrorDialogComponent,
    SuccessDialogComponent,
    ConfirmDialogComponent,
    InformationDialogComponent,
    IdentificationDialogComponent,
    SavePersonneComponent,
    MainComponent,
    MainDeclarationComponent,
    NewdeclarationComponent,
    WaitingDialogComponent,
    IdentifierDeclarantComponent,
    ViewPersonneBoxComponent,
    IdentificationNaissanceComponent,
    ArchiverDocumentDeclarationComponent,
    UploadFileDialogComponent,
    ConsultDeclarationDialogComponent,
    DeclarationBoxComponent,
    ResumeDeclarationComponent,
    ActeNaissanceViewBoxComponent,
    ActeDecesViewBoxComponent,
    ActeMariageViewBoxComponent,
    AviserDeclarationComponent,
    TableDeclarationsComponent,
    AuthentifierDeclarationComponent,
    MainImpressionComponent,
    ImprimerDocumentComponent,
    MainArchivageAutresComponent,
    MainProcurationComponent,
    EditProcurationComponent,
    ListProcurationComponent,
    EditProcurationNaissanceComponent,
    EditProcurationDecesComponent,
    EditProcurationSpecialeComponent,
    ViewProcurationComponent,
    ViewProcurationNaissanceComponent,
    ViewProcurationDecesComponent,
    ConsultProcurationDialogComponent,
    MainAttestationComponent,
    EditAttestationComponent,
    EditAttestationNaissanceComponent,
    EditAttestationDecesComponent,
    ConsultAttestationDialogComponent,
    IdentificationDecesComponent,
    IdentificationMariageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents:[
    ConfirmDialogComponent, 
    ErrorDialogComponent, 
    InformationDialogComponent, 
    SuccessDialogComponent,
    IdentificationDialogComponent,
    SavePersonneComponent,
    WaitingDialogComponent,
    UploadFileDialogComponent,
    ConsultDeclarationDialogComponent,
    ResumeDeclarationComponent,
    ConsultProcurationDialogComponent,
    ConsultAttestationDialogComponent
  ]
})
export class AppModule { }
