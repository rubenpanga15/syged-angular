import { Personne } from '../personne';

export class UserAdapter {

    id: number;
    username: string;
    password: string;
    matricule: string;
    role: string;
    token: string;
    personne: Personne;
    statut: boolean;
    dateCreat: Date;
    fkCentre: string;

}
