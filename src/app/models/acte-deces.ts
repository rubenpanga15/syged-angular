export class ActeDeces {
    numero: string;
    fkPere: number;
    fkMere: number;
    lieuDeces: string;
    dateDeces: Date;
    etatCivil: string;
    fkDeclaration: string;
    statut: boolean;
    dateCreat: Date;
    circonstance: string;
}
