export class Procedure {
    id: number;
    description: string;
    isPayant: boolean;
    type: string;
    statut: boolean;
    dateCreat: Date;
}
