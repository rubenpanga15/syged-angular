import { Personne } from './personne';
import { User } from './user';

export class ProcurationDeces {
    numero:string;
    interesse: number;
    interessePersonne: Personne;
    fkPere: number;
    pere: Personne;
    fkMere: number;
    mere: Personne;
    fkDefunt: number;
    defunt: Personne;
    divorceLe: Date;
    dateDivorce: string 
    mandataire: number;
    mandatairePersonne: Personne;
    agentCreat: number;
    agentCreatPersonne: User;
    dateCreat: Date;
    statut: boolean;
    fkCentre: string;

    preuvePaiement: string;
    agentImpression: number;
    dateImpression: Date;
    isImprimer: boolean;
}
