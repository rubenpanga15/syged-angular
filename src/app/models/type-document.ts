export class TypeDocument {
    id: number;
    description: string;
    aScanner: boolean;
    statut: boolean;
    dateCreat: Date;
}
