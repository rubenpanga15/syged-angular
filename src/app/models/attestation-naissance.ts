import { Personne } from './personne';
import { User } from './user';

export class AttestationNaissance {
    numero: string
    lieuNaissance: string;
    dateNaissance: Date;
    fkPere: number;
    fkMere: number;
    fkEnfant: number;
    fkCentre: string;
    agentCreat: number;
    statut: boolean;
    dateCreat: Date;

    pere: Personne;
    mere: Personne;
    enfant: Personne;
    agent: User;

    preuvePaiement: string;
    agentImpression: number;
    dateImpression: Date;
    isImprimer: boolean;
}
