import { ActeDeces } from './acte-deces';
import { ActeMariage } from './acte-mariage';
import { ActeNaissance } from './acte-naissance';
import { Document } from './document';
import { Personne } from './personne';
import { UserAdapter } from './utilities/user-adapter';

export class Declaration {
    static TYPE_ACTE_NAISSANCE: string = 'ACTE DE NAISSANCE';
    static TYPE_ACTE_DECES:string = 'ACTE DE DECES';
    static TYPE_ACTE_MARIAGE:string = 'ACTE DE MARIAGE';

    numero: string;
    fkBeneficiare: number;
    fkRequerant: number;
    langue: string;
    fkInterprete: number;
    fkRedacteur: number;
    observationRedacteur: string;
    fkPrepose: number;
    observationPrepose: string;
    fkOec: number;
    observationOec: string;
    avis: string;
    type: string;
    dateAvis: Date;
    statut: boolean;
    dateCreat: Date;

    beneficiaire: Personne;
    requerant: Personne;
    interprete: Personne;
    redacteur: UserAdapter;
    prepose: UserAdapter;
    oec: UserAdapter;

    documents: Document[]
    acteMariage: ActeMariage;
    acteDeces: ActeDeces;
    acteNaissance: ActeNaissance;

}
