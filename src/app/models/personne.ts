export class Personne {
    id: number;
    nom: string;
    postnom: string;
    prenom: string;
    lieuNaissance: string;
    dateNaissance: Date;
    sexe: string;
    telephone: string;
    email: string;
    profession: string;
    etatCivil: string;
    nomPere: string;
    nomDeLaMere: string;
    province: string;
    district: string;
    secteur: string;
    groupement: string;
    adresse: string;
    statut: boolean;
    dateCreat: Date
}
