import { TypeDocument } from './type-document';

export class DocumentProcedure {
    id: number;
    fkTypeDocument: number;
    isObligatoire: boolean;
    fkProcedure: number;
    statut: number;
    dateCreat: Date;

    typeDocument: TypeDocument
}
