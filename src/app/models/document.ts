import { LienArchive } from './lien-archive';
import { TypeDocument } from './type-document';

export class Document {
    numero: string;
    numeroRef: string;
    dateDocument: Date;
    fkTypeDocument: number;
    fkDeclaration: string;
    statut: boolean;
    dateCreat: Date;

    lienArchives: LienArchive[]
    typeDocument: TypeDocument;
}
