export class User {
    id: number;
    username: string;
    password: string;
    matricule: string;
    role: string;
    token: string;
    fkPersonne: number;
    statut: boolean;
    dateCreat: Date;
    fkCentre: string;
}
