export class LienArchive {

    id: number;
    path: string;
    fkDocument: number;
    statut: boolean;
    dateCreat: Date;
}
