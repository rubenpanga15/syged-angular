import { Personne } from './personne';
import { User } from './user';

export class ProcurationNaissance {
    numero:string;
    interesse: number;
    interessePersonne: Personne;
    fkPere: number;
    pere: Personne;
    fkMere: number;
    mere: Personne;
    fkEnfant: number;
    enfant: Personne;
    situationMatrimoniale: string;
    divorceLe: Date;
    dateDivorce: string 
    mandataire: number;
    mandatairePersonne: Personne;
    personneContact: number;
    personneContactPersonne: Personne;
    agentCreat: number;
    agentCreatPersonne: User;
    dateCreat: Date;
    statut: boolean;
    fkCentre: string;

    preuvePaiement: string;
    agentImpression: number;
    dateImpression: Date;
    isImprimer: boolean;
}
