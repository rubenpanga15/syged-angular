import { Personne } from './personne';

export class ActeNaissance {
    numero: string;
    dateNaissance: Date;
    fkPere: number;
    fkMere: number;
    fkDeclaration: string;
    statut: boolean;
    dateCreat: Date;
    lieuNaissance: string

    enfant: Personne
    pere: Personne;
    mere: Personne;
}
