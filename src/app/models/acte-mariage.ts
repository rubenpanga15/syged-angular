export class ActeMariage {
    numero: string;
    regimeMatrimonial: string;
    montantDot: number;
    fkPereEpouse: number;
    fkMereEpouse: number;
    fkPereEpoux: number;
    fkMereEpoux: number;
    lieuPublicite: string;
    fkDeclaration: string;
    statut: boolean;
    dateCreat: Date;
    dateMariage: Date;
}
