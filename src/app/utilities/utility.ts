import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { IdentifierDeclarantComponent } from '../components/declaration/identifier-declarant/identifier-declarant.component';
import { ResumeDeclarationComponent } from '../components/resume-declaration/resume-declaration.component';
import { ConfirmDialogComponent } from '../dialogs/confirm-dialog/confirm-dialog.component';
import { ErrorDialogComponent } from '../dialogs/error-dialog/error-dialog.component';
import { IdentificationDialogComponent } from '../dialogs/identification-dialog/identification-dialog.component';
import { InformationDialogComponent } from '../dialogs/information-dialog/information-dialog.component';
import { SuccessDialogComponent } from '../dialogs/succes-dialog/succes-dialog.component';
import { UploadFileDialogComponent } from '../dialogs/upload-file-dialog/upload-file-dialog.component';

export class Utility {

    static openIdentificationDialog(dialog: MatDialog): MatDialogRef<IdentificationDialogComponent>{
        return dialog.open(IdentificationDialogComponent, {
            width: '90%',
            height: '90%'
        });
    }

    static openInfoDialog(dialog: MatDialog, message: string): void{
        dialog.open(InformationDialogComponent, {data: message})
    }

    static openErrorDialog(dialog: MatDialog, message: string): void{
        dialog.open(ErrorDialogComponent, {data: message})
    }

    static openConfirmDialog(dialog: MatDialog, message: string): MatDialogRef<ConfirmDialogComponent>{
        return dialog.open(ConfirmDialogComponent, {data: message});
    }

    static openSuccessDialog(dialog: MatDialog, message: string): void{
        dialog.open(SuccessDialogComponent, {data: message})
    }

    static openUploadFileDialog(dialog: MatDialog): MatDialogRef<UploadFileDialogComponent>{
        return dialog.open(UploadFileDialogComponent);
    }
}
