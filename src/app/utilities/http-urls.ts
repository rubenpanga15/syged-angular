export class HttpURLs {
    private static SCHEMAS: string = 'http';
    private static IP: string = 'localhost';
    private static PORT: string = '8089';
    
    private static getBase(): string{
        return HttpURLs.SCHEMAS + '://' + HttpURLs.IP + ':' + HttpURLs.PORT + '/SyGEDWeb/';
    }

    static URL_CONNEXION: string = HttpURLs.getBase() + 'connexion';
    static URL_SEARCH_PERSON: string = HttpURLs.getBase() + 'api/identification/search-person';
    static URL_GET_DOCUMENT_NECESSAIRE = HttpURLs.getBase() + 'api/archivage/recuperer-document';
    static URL_SAVE_DECLARATION = HttpURLs.getBase() + 'api/save/declaration';
    static URL_UPLOAD_FICHIERS = HttpURLs.getBase() + 'api/archivage/upload-fichiers';
    static URL_RECUPERER_DOSSIER = HttpURLs.getBase() + 'api/declaration/recuperer-dossier';
    static URL_RECUPERER_DOSSIER_PAR_PERIODE = HttpURLs.getBase() + 'api/declaration/recuperer-dossier/periode';
    static URL_AVISER_DECLARATION = HttpURLs.getBase() + 'api/declaration/valider';
    static URL_AUTHENTIFIER_ACTE = HttpURLs.getBase() + 'api/declaration/authentification-acte';
    static URL_SAVE_PROCURATION: string = HttpURLs.getBase() + 'api/procuration/save';
    static URL_SAVE_ATTESTATION: string = HttpURLs.getBase() + 'api/save/attestation';
    static URL_GET_PROCEDURES: string = HttpURLs.getBase() + 'api/configuration/list-procedure';
}
