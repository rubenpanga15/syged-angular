import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ArchiverDocumentDeclarationComponent } from './components/declaration/archiver-document-declaration/archiver-document-declaration.component';
import { IdentificationNaissanceComponent } from './components/declaration/identification-naissance/identification-naissance.component';
import { IdentifierDeclarantComponent } from './components/declaration/identifier-declarant/identifier-declarant.component';
import { MainDeclarationComponent } from './components/declaration/main-declaration/main-declaration.component';
import { NewdeclarationComponent } from './components/declaration/newdeclaration/newdeclaration.component';
import { LoginComponent } from './components/login/login.component';
import { MainComponent } from './components/main/main.component';
import { ResumeDeclarationComponent } from './components/resume-declaration/resume-declaration.component';
import { AviserDeclarationComponent } from './components/declaration/aviser-declaration/aviser-declaration.component';
import { AuthentifierDeclarationComponent } from './components/declaration/authentifier-declaration/authentifier-declaration.component';
import { AuthentificationGuard } from './guards/authentification.guard';
import { MainImpressionComponent } from './components/impression/main-impression/main-impression.component';
import { ImprimerDocumentComponent } from './components/impression/imprimer-document/imprimer-document.component';
import { MainArchivageAutresComponent } from './components/archivage-autres/main-archivage-autres/main-archivage-autres.component';
import { MainProcurationComponent } from './components/archivage-autres/procuration/main-procuration/main-procuration.component';
import { EditProcurationComponent } from './components/archivage-autres/procuration/edit-procuration/edit-procuration.component';
import { ListProcurationComponent } from './components/archivage-autres/procuration/list-procuration/list-procuration.component';
import { EditProcurationNaissanceComponent } from './components/archivage-autres/procuration/edit-procuration-naissance/edit-procuration-naissance.component';
import { EditProcurationDecesComponent } from './components/archivage-autres/procuration/edit-procuration-deces/edit-procuration-deces.component';
import { EditProcurationSpecialeComponent } from './components/archivage-autres/procuration/edit-procuration-speciale/edit-procuration-speciale.component';
import { MainAttestationComponent } from './components/archivage-autres/attestation/main-attestation/main-attestation.component';
import { EditAttestationComponent } from './components/archivage-autres/attestation/edit-attestation/edit-attestation.component';
import { EditAttestationNaissanceComponent } from './components/archivage-autres/attestation/edit-attestation-naissance/edit-attestation-naissance.component';
import { EditAttestationDecesComponent } from './components/archivage-autres/attestation/edit-attestation-deces/edit-attestation-deces.component';
import { IdentificationDecesComponent } from './components/declaration/identification-deces/identification-deces.component';
import { IdentificationMariageComponent } from './components/declaration/identification-mariage/identification-mariage.component';


const routes: Routes = [
  {
    path: 'connexion', component: LoginComponent
  },
  {
    path: '', redirectTo: 'connexion', pathMatch:'full'
  },
  {
    path: 'main', component: MainComponent, canActivate: [AuthentificationGuard],
    children:[
      {
        path: '', component: DashboardComponent
      },
      {
        path: 'declaration', component: MainDeclarationComponent,
        children:[
          {
            path: 'new', component: NewdeclarationComponent,
            children:[
              {
                path: '', component: IdentifierDeclarantComponent
              },
              {
                path: 'naissance', component: IdentificationNaissanceComponent
              },
              {
                path: 'deces', component: IdentificationDecesComponent
              },
              {
                path: 'mariage', component: IdentificationMariageComponent
              },
              {
                path: 'archiver', component: ArchiverDocumentDeclarationComponent
              },
              {
                path: 'resume', component: ResumeDeclarationComponent
              }
            ]
          },
          {
            path: 'avis', component: AviserDeclarationComponent
          },
          {
            path: 'authentifier', component: AuthentifierDeclarationComponent
          }
        ]
      },
      {
        path: 'impression', component: MainImpressionComponent,
        children:[
          {
            path: 'imprimer', component: ImprimerDocumentComponent
          }
        ]
      },
      {
        path: 'archivage-autres', component: MainArchivageAutresComponent,
        children:[
          {
            path: 'procuration', component: MainProcurationComponent, 
            children:[
              {
                path: 'edit', component: EditProcurationComponent,
                children:[
                  {
                    path: 'procuration-naissance', component: EditProcurationNaissanceComponent
                  },
                  {
                    path: 'procuration-deces', component: EditProcurationDecesComponent
                  },
                  {
                    path: 'procuration-speciale', component: EditProcurationSpecialeComponent
                  }
                ]
              },
              {
                path: 'list', component: ListProcurationComponent
              }
            ]
          },
          {
            path: 'attestation' ,component: MainAttestationComponent,
            children:[
              {
                path: 'edit', component: EditAttestationComponent,
                children:[
                  {
                    path: 'attestation-naissance', component: EditAttestationNaissanceComponent
                  },
                  {
                    path: 'attestation-deces', component: EditAttestationDecesComponent
                  },
                  {
                    path: 'attestation-mariage', component: EditAttestationNaissanceComponent
                  }
                ]
              }
            ]
          }
        ]
      }
    ]
  },
  // {
  //   path: '**', redirectTo: 'connexion'
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
